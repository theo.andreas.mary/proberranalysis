# ProbErrAnalysis

MATLAB codes to perform the numerical experiments reported in the article 
"A New Probabilistic Approach to Rounding Error Analysis", N. J. Higham and T. Mary.

## Included MATLAB files
* **gamma_bound.m**: function called by most other codes to compute the deterministic and probabilistic error bounds.
* **plot_error.m**: function called by most other codes to plot the error and its bounds.
* **inner_product.m**: generates Figures 4.1a, 4.1b, and 4.4a.
* **inner_product_lowerprec.m**: generates Figure 4.2.
* **inner_product_verylarge.m**: generates Figure 4.3.
* **inner_product_breakpoints.m**: generates Figure 4.4b.
* **matvec_product.m**: generates Figure 4.6.
* **linear_system.m**: generates Figure 4.7.
* **linear_system_suitesparse.m**: generates Figure 4.8.
* **plambda.m**: generates Table 2.1.
* **qlambda_n.m**: generates Table 3.1.

## Requirements
* The codes have been developed and tested with MATLAB 2018b.
* The script **inner_product_lowerprec.m** requires the function **chop.m**
from the [Matrix Computations Toolbox](http://www.ma.man.ac.uk/~higham/mctoolbox/).
* The script **linear_system_suitesparse.m** requires the 
[ssget package](https://github.com/jluttine/suitesparse/tree/master/ssget).
* The script **qlambda_n.m** requires the [Advanpix Multiprecision Computing Toolbox](https://www.advanpix.com/).

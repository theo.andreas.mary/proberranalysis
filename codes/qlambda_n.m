% QLAMBDA_N
% Generates Table 3.1
% Requires the Advanpix Multiprecision Computing Toolbox (https://www.advanpix.com/)

prec = 's';
np = 6:10; 
nvals = 10.^np;
nn = length(nvals);

lambda_vals = [10:0.5:13];
nlam = length(lambda_vals);

probs = zeros(nlam,nn);

for i = 1:nlam
  lambda = lambda_vals(i);
  fprintf('%2.1f ',lambda)
  for j = 1:nn
      probs(i,j) =  qlambdan(lambda,nvals(j)^3/3,prec);
      fprintf('& $%9.4e$ ',probs(i,j))
  end    
  fprintf('\\\\\n')
end

function f = qlambdan(lambda,n,prec)
% QLAMBDAN
% qlambdan(lambda,n,prec) computes Q(lambda,n)=1-n(1-P(lambda)).
% prec is the precision: 'h', 's', 'd', or 'q'.
% The computation is in mp arithmetic.

switch prec(1)
  case 'h'
    p = 11;
  case 's'
    p = 24;
  case 'd'
    p = 53;
  case 'q' 
    p = 113;
end
u = mp(2^(-p),100);
x = (1-u)^2;
f = 1-n*2*exp(-(lambda^2/2)*x);

end

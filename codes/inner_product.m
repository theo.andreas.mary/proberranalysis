% INNER_PRODUCT
% Generates Figures 4.1 and 4.4a

clear all;
close all;

u      = 2^-24;
nlist  = round(logspace(2,5));
lambda = 1;

for datatype = [1 2 3]
  rng(1)
  ntest = 100;
  nbfailures = 0;
  err = zeros(length(nlist),1);
  errmax = zeros(length(nlist),1);
  erravg = zeros(length(nlist),1);
  detbound = zeros(length(nlist),1);
  probound = zeros(length(nlist),1);

  for itest=1:ntest
    for j=1:length(nlist)
      n = nlist(j);
      switch(datatype)
      case 1
        a = rand(n,1); 
        b = rand(n,1); 
        ylm = [5e-8 1e-2];
        ytk = 10.^(-7:-2);
      case 2
        a = (rand(n,1)-0.5)*2; 
        b = (rand(n,1)-0.5)*2; 
        ylm = []; ytk = [];
      case 3
        a = ones(n,1)*rand(); 
        b = ones(n,1)*rand(); 
        ylm = [5e-7 1e-2];
        ytk = 10.^(-6:-2);
      end
      a = single(a); b = single(b);

      xd = double(a)'*double(b);
      xs = 0;
      for i=1:n
        xs = xs + a(i)*b(i);
      end
      xs = double(xs);
      err(j) = abs(xs-xd)/(double(abs(a))'*double(abs(b)));
      detbound(j) = gamma_bound(u, n, 'det', lambda);
      probound(j) = gamma_bound(u, n, 'pro', lambda);
      if err(j) > probound(j)
        nbfailures = nbfailures+1;
      end
    end
    errmax = max(errmax,err);
    erravg = erravg + err;
  end
  erravg = erravg/ntest;
  
  plot_error(nlist,detbound,probound,errmax,erravg,ylm,ytk);

end

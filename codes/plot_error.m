% PLOT_ERROR
% Plots backward error and its bounds - used by all other scripts generating figures
% Inputs:
%    nlist    = list of problem sizes n
%    detbound = deterministic bound 
%    probound = probabilistic bound 
%    errmax   = maximum backward error
%    erravg   = mean backward error
%    ylm, ytk, xlb, xtk = ylim, yticks, xlabel, xticks optional arguments
%    Axb      = are the bounds for Ax=b?
function plot_error(nlist,detbound,probound,errmax,erravg,ylm,ytk,xlb,xtk,Axb)
if nargin < 10, Axb = 0; end
if nargin < 9, xtk = []; end
if nargin < 8, xlb = 'n'; end
if nargin < 7, ytk = []; end
if nargin < 6, ylm = []; end
    
  fs=16; ms=7; lw=1;
  fig=figure();
  loglog(nlist,detbound,'-o','Markersize',ms);
  hold on;
  loglog(nlist,probound,'-v','Markersize',ms);
  if isempty(erravg)
    loglog(nlist,errmax,'s','Markersize',ms);
    if Axb
      gammad = sprintf('$$3\\gamma_%s+\\gamma_%s^2$$',xlb,xlb);
      gammap = sprintf('$$3\\widetilde{\\gamma}_%s(\\lambda)+\\widetilde{\\gamma}_%s(\\lambda)^2$$',xlb,xlb);
    else
      gammad = sprintf('$$\\gamma_%s$$',xlb);
      gammap = sprintf('$$\\widetilde{\\gamma}_%s(\\lambda)$$',xlb);
    end
    hleg = legend(gammad,gammap,'$$\varepsilon_{bwd}$$');
  else
    loglog(nlist,errmax,'-s','Markersize',ms);
    loglog(nlist,erravg,'-x','Markersize',ms);
    if Axb
      gammad = sprintf('$$3\\gamma_%s+\\gamma_%s^2$$',xlb,xlb);
      gammap = sprintf('$$3\\widetilde{\\gamma}_%s(\\lambda)+\\widetilde{\\gamma}_%s(\\lambda)^2$$',xlb,xlb);
    else
      gammad = sprintf('$$\\gamma_%s$$',xlb);
      gammap = sprintf('$$\\widetilde{\\gamma}_%s(\\lambda)$$',xlb);
    end
    hleg = legend(gammad, gammap,'$$\varepsilon^{max}_{bwd}$$','$$\varepsilon^{mean}_{bwd}$$');
  end
  xlabel(sprintf('$$%s$$',xlb),'interpreter','latex');
  xlim([nlist(1) nlist(end)]);
  set(gca,'fontsize',fs);
  set(hleg,'fontsize',fs,'interpreter','latex','location','northwest');
  if ~Axb, set(hleg,'orientation','horizontal'), end
  if ~isempty(ylm), ylim(ylm), end
  if ~isempty(ytk), yticks(ytk), end
  if ~isempty(xtk), xticks(xtk), end
end

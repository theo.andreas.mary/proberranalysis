% MATVEC_PRODUCT
% Generates Figure 4.6

clear all;
close all;

u = 2^-24;
nlist  = round(logspace(2,4));
lambda = 1;

for datatype = [1 2]
  rng(1)
  ntest = 10;
  nbfailures = 0;
  err = zeros(length(nlist),1);
  errmax = zeros(length(nlist),1);
  erravg = zeros(length(nlist),1);
  detbound = zeros(length(nlist),1);
  probound = zeros(length(nlist),1);
  for itest=1:ntest
    for j=1:length(nlist)
      n = nlist(j);
       fprintf('%g of %g nvals (n = %g), %g of %g repetitions\n',...
               j,length(nlist),n,itest,ntest)
      switch(datatype)
      case(1)
        A = rand(n); 
        x = rand(n,1); 
      case(2)
        A = (rand(n)-0.5)*2; 
        x = (rand(n,1)-0.5)*2; 
      end
      A = single(A); x = single(x);

      yd = double(A)*double(x);

      ys = zeros(n,1);
      for ii = 1:n
          for jj = 1:n
              ys(ii) = ys(ii) + A(ii,jj)*x(jj);
          end
      end    
      ys = double(ys);

      absAx  = abs(double(A))*abs(double(x));
      absres = abs(ys-yd);
      err(j) = max(absres./absAx);
      detbound(j) = gamma_bound(u, n, 'det', lambda);
      probound(j) = gamma_bound(u, n, 'pro', lambda);
      if err(j) > probound(j)
        nbfailures = nbfailures + 1;
      end
    end
    errmax = max(errmax,err);
    erravg = erravg + err;
  end
  erravg = erravg/ntest;

  plot_error(nlist, detbound, probound, errmax, erravg);
end

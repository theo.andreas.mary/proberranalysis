% LINEAR_SYSTEM
% Generates Figure 4.7

clear all;
close all;

u = 2^(-24);
nlist  = round(logspace(1,4,20));
lambda = 1;

for datatype=[1 2]
  rng(1)
  ntest = 10;
  err = zeros(length(nlist),1);
  errmax = zeros(length(nlist),1);
  erravg = zeros(length(nlist),1);
  nbfailures = 0;
  for itest=1:ntest
    for j=1:length(nlist)
      n = nlist(j);
      fprintf('%g of %g nvals (n = %g), %g of %g repetitions\n',...
              j,length(nlist),n,itest,ntest)
      switch(datatype)
      case(1)
        A = rand(n); 
        b = rand(n,1);
      case(2)
        A = rand(n); 
        b = A*rand(n,1);
      end
      A = single(A); b = single(b);

      [L,U] = lu(A);
      x = double(U\(L\b));
      absLUx = abs(double(L))*(abs(double(U))*abs(double(x)));
      absres = abs(double(A)*double(x)-double(b));
      err(j) = max(absres./absLUx);
      gamma_det = gamma_bound(u,n,'det',lambda);
      gamma_pro = gamma_bound(u,n,'pro',lambda);
      detbound(j)=3*gamma_det + gamma_det^2;
      probound(j)=3*gamma_pro + gamma_pro^2;
      if err(j) > probound(j)
        nbfailures = nbfailures+1;
      end
    end
    errmax = max(errmax,err);
    erravg = erravg + err;
  end
  erravg = erravg/ntest;

  plot_error(nlist, detbound, probound, errmax, erravg, [], [], 'n', [], 1);
end

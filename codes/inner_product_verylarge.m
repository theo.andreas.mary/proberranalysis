% INNER_PRODUCT_VERYLARGE
% Generates Figure 4.3

clear all;
close all;

u      = 2^-24;
n      = 10^8;
lambda = 1;

err      = zeros(n,1);
detbound = zeros(n,1);
probound = zeros(n,1);

rng(1)
a = rand(n,1); 
b = rand(n,1); 
a = single(a); b = single(b);

xs = 0; xd = 0;
for i=1:n
  p  = a(i)*b(i);
  q  = xs;
  xs = q + p;
  xs_exact = double(q)+double(p);
  delta(i) = (double(xs)-xs_exact)/xs_exact;

  xd = xd + double(a(i))*double(b(i));
  err(i)      = abs(xd-double(xs))/abs(xd);
  detbound(i) = gamma_bound(u, i, 'det', lambda);
  probound(i) = gamma_bound(u, i, 'pro', lambda);

  progress = i/n*100;
  if (round(progress) == progress), fprintf('...%d%%\n',progress); end
end


% plot only subset of n points to speed up figure generation
I=1:n; Iplot = unique(round(logspace(0,8,10000)));
xtk = 10.^(0:2:8);
plot_error(I(Iplot),detbound(Iplot),probound(Iplot),err(Iplot),[],[],[],'i',xtk);

figure()
subplot(2,1,1)
histogram(delta(1:1e6),100);
xticks((-6:6).*1e-8);
set(gca,'fontsize',12);
subplot(2,1,2)
histogram(delta(1e6:n),100);
xticks((-6:6).*1e-8);
set(gca,'fontsize',12);

% INNER_BREAKPOINTS
% Generates Figure 4.4b

clear all;
close all;
fs=16; lw=1;

nlist = round(logspace(2,4,250));
n = nlist(end);

j = 0; breakpoints = []; jbreakpoints = [];
logxs_old = -inf;

rng(1);
a = ones(n,1)*rand(); a = single(a);
b = ones(n,1)*rand(); b = single(b);
xd = double(a(1))*double(b(1)); 
absatb = abs(double(a).*double(b));
xs = a(1)*b(1);
for i=1:n-1
  xd = xd + double(a(i+1))*double(b(i+1));
  xs = xs + a(i+1)*b(i+1);
  logxs_new = floor(log2(double(xs)));
  if logxs_new > logxs_old
    breakpoints  = [breakpoints, i];
    jbreakpoints = [jbreakpoints, max(1,j)];
  end
  logxs_old = logxs_new;
  if i+1 >= nlist(j+1)
    j = j+1;
    err(j) = abs(double(xs)-xd)/sum(absatb(1:i+1));
  end
end

fig=figure();
plot(nlist,err,'-x');
hold on;
xlabel('$$i$$','interpreter','latex');
set(gca,'fontsize',fs);
ymin = min(err);
for j=1:length(breakpoints)
  bp = breakpoints(j);
  line([bp bp],[ymin err(jbreakpoints(j))],'Color','k','LineStyle','--','LineWidth',lw);
end


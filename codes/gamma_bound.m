% GAMMA_BOUND
% Computes bound gamma_n 
% Inputs: 
%    u      = unit roundoff
%    n      = problem size
%    type   = deterministic ('det') or probabilistic ('pro') bound
%    lambda = used for probabilistic bound
% Outputs:
%    gamma = the bound
function gamma = gamma_bound(u,n,type,lambda)
  if strcmp(type,'det')
    gamma = min(1,n*u/(1-n*u));
    if n*u >= 1 
      gamma = 1;
    end
  elseif strcmp(type,'pro')
    if lambda <= 0 
      error('lambda <= 0');
    end
    gamma = exp(lambda*sqrt(n)*u + n*u^2/(1-u)) - 1;
    gamma = min(gamma,1);
  else
    error('Wrong type');
  end
end

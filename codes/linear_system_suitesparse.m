% LINEAR_SYSTEM_SUITESPARSE
% Generates Figure 4.8
% Requires ssget package (https://github.com/jluttine/suitesparse)

clear all;
close all;

rng(1);
u = 2^-53;
lambda = 1;

index     = ssget;
indlist   = find(index.nrows>=10 & index.nrows<=10000 ...
                                 & index.nrows==index.ncols);
[nlist,i] = sort(index.nrows(indlist)) ;
indlist   = indlist(i);

nn          = length(indlist);
filterLU    = zeros(nn,1);
filterAxb   = zeros(nn,1);
berrLU      = zeros(nn,1);
detboundLU  = zeros(nn,1);
proboundLU  = zeros(nn,1);
berrAxb     = zeros(nn,1);
detboundAxb = zeros(nn,1);
proboundAxb = zeros(nn,1);

for j=1:nn
  Problem = ssget(indlist(j));
  A = full(Problem.A);
  n = nlist(j);
  if isfield(Problem,'b')
    b = Problem.b;
  else
    b = rand(n,1);
  end
  fprintf('n = %g\n',n)

  [L,U] = lu(A);
  absLU = abs(L)*abs(U);
  berrLU(j) = max(max(abs(A-L*U)./absLU));
  detboundLU(j)  = gamma_bound(u, n, 'det', lambda);
  proboundLU(j)  = gamma_bound(u, n, 'pro', lambda);
  if berrLU(j) > detboundLU(j) 
    % error exceeds even deterministic bound => underflow problem?
    % => filter it out
    filterLU(j)  = 1;
    filterAxb(j) = 1;
    continue
  end

  x = U\(L\b);
  if any(isnan(x)+isinf(x)) 
    % matrix too ill-conditioned to provide good solution in double precision
    % => filter it out
    filterAxb(j) = 1;
    continue
  end
  berrAxb(j) = max(abs(A*x-b)./(absLU*abs(x)));
  detboundAxb(j) = 3*detboundLU(j) + detboundLU(j)^2;
  proboundAxb(j) = 3*proboundLU(j) + proboundLU(j)^2;
end

IfilterLU   = (filterLU==0);
IfilterAxb  = (filterAxb==0);
nlistLU     = nlist(IfilterLU);
nlistAxb    = nlist(IfilterAxb);
berrLU      = berrLU(IfilterLU);
detboundLU  = detboundLU(IfilterLU);
proboundLU  = proboundLU(IfilterLU);
berrAxb     = berrAxb(IfilterAxb);
detboundAxb = detboundAxb(IfilterAxb);
proboundAxb = proboundAxb(IfilterAxb);

ylm = [1e-17 1e-12];
ytk = 10.^(-17:-12);
plot_error(nlistLU, detboundLU, proboundLU, berrLU, [], ylm, ytk);

ylm = [4e-18 1e-12];
ytk = 10.^(-18:-12);
plot_error(nlistAxb, detboundAxb, proboundAxb, berrAxb, [], ylm, ytk, 'n', [], 1);

% INNER_PRODUCT_LOWERPREC
% Generates Figure 4.2
% Requires chop.m from the Matric Computation Toolbox

clear all;
close all;

for t = [11 4]
  u = 2^(-t);
  if t==11
    nlist  = round(logspace(1,3));
    ylm = [3e-4 1];
  else
    ilist = linspace(log10(5), log10(100), 50);
    nlist = round(10.^ilist);
    ylm = [];
  end
  nn = length(nlist);

  rng(1)
  lambda = 1;
  ntest  = 100;
      
  err    = zeros(nn,ntest);
  errmax = zeros(nn,1);
  erravg = zeros(nn,1);
  probound = zeros(nn,1);
  detbound = zeros(nn,1);
  nbfailures = 0;
  for i=1:nn
  
      n = nlist(i);
      detbound(i) = gamma_bound(u, n, 'det', lambda);
      probound(i) = gamma_bound(u, n, 'pro', lambda);
  
     for j=1:ntest
         fprintf('%g of %g nvals (n = %g), %g of %g repetitions\n',i,nn,n,j,ntest)
  
         a = rand(n,1); b = rand(n,1); 
         a = chop(a,t); b = chop(b,t);
         ad = double(a); bd = double(b);
         xd = ad'*bd;
         xh = 0;
         for k=1:n
           xh = chop(xh + chop(a(k)*b(k),t), t);
         end
         xh = double(xh);
  
         err(i,j) = abs(xh-xd)/( abs(ad)'*abs(bd) );
         if err(i,j) > probound(i)
            nbfailures = nbfailures + 1;
         end
      end
      errmax(i) = max(err(i,:));
      erravg(i) = sum(err(i,:));
  end
  erravg = erravg/ntest;
  
  plot_error(nlist, detbound, probound, errmax, erravg, ylm);

end

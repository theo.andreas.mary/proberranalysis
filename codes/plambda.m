% PLAMBDA
% Generates Table 2.1

u16 = 2^(-11);
u64 = 2^(-53);

for lambda = 2:5
    fprintf('%2.0f &  %5.4f & %5.4f\\\\\n', lambda, prob(lambda,u16), prob(lambda,u64))
end    

function f = prob(lambda, u)
x = (1-u)^2;
f = 1 - 2*exp(-(lambda^2/2)*x);
end
